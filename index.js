// console.log("Hello World");

const getCube = (number) => Math.pow(number, 3);

let cube = 2;
console.log(`The cube of ${cube} is ${getCube(cube)} `);

const address = ["258", "Washington Ave", "NW", "California", "90011"];
const [houseNumber, street, initial, state, postalCode] = address;
console.log(`I live at ${houseNumber} ${street} ${initial}, ${state} ${postalCode}`);

const animals = {
    name: 'Lolong',
    species: 'crocodile',
    weight: '1075 kgs',
    measurement: '20 ft 3in'
}
const {name:animalName, species:animalSpecies, weight:animalWeight, measurement:animalMeasuerment} = animals;
console.log(`${animalName} was a saltwater ${animalSpecies}. He weighed at ${animalWeight} with a measurement of ${animalMeasuerment}.`);

const lotteryNumber = [1, 2, 3, 4, 5];

lotteryNumber.forEach((number) => console.log(number));

 const reduceNumber = () => lotteryNumber.reduce((sum, number) => (sum += number));

 console.log(reduceNumber());

 class Dog{
    constructor (name, age, breed){
        this.dogName = name;
        this.dogAge = age;
        this.dogBreed = breed;
    }
 }
 let dog = new Dog("Frankie", 5, "Miniature Dashshund");
 console.log(dog);